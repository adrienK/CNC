# ANYCUBIC kossel linear plus
 *Avec carte MKS sbase sous smoothieware*
 
## Configuration
 *Fichier de conf: ![conf](https://git.iglou.eu/adrien/CNC/src/branch/master/AnycubicKosselLP/config.txt)*
 *Utilisations de toutes les features de base (dons l'autoleveling)*
 
 **En mode 32 pas !**
 **Connection RJ45 activé, en mode auto (dhcp)**
 **Volumetric extrusion définie pour 1.75mm**
 
### Liste des pins
 **Axe X:** alpha (*2.0)
 **Axe Y:** beta (*2.1)
 **Axe Z:** gamma (*2.2)
 **Extrudeur:** delta (*2.3)
 
 **Sonde tête:** 0.23
 **Chauffe tête:** 2.7
 
 **Sonde bed:** 0.24
 **Chauffe bed:** 2.5
 
 **EndStop x:** 1.25
 **EndStop Y:** 1.27
 **EndStop Z:** 1.29
 
 **Autoleveling:** 1.28
 
 **Fan 0:** 0.26
 **Fan 2:** 2.4
 
 **Ecran:** [12864](https://gloimg.gbtcdn.com/gb/2015/201507/goods-img/1502128558939507760.jpg)
 **Connecteurs:** EXP1 / EXP2
 
 ![used pin](https://git.iglou.eu/adrien/CNC/raw/branch/master/AnycubicKosselLP/Mks-Sbase%20used%20Layout.jpg)
 
## Kossel
**Moteurs:**
 ![moteur](https://git.iglou.eu/adrien/CNC/raw/branch/master/AnycubicKosselLP/61g-KazhWLL._SL1000_.jpg)
 
## MKS Sbase
**Les pins:**
 ![pins](https://git.iglou.eu/adrien/CNC/raw/branch/master/AnycubicKosselLP/Mks-Sbase%20v1.2%20Pin%20Layout.jpg)
 
**Branchement ecran:**
 ![screen](https://git.iglou.eu/adrien/CNC/raw/branch/master/AnycubicKosselLP/MKS-SBASE-V1-3-CE-et-RoHS-32bit-Bras-plate-forme-Lisse-contr-le-conseil-open.jpg)